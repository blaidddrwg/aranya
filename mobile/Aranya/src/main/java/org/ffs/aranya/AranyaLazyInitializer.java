package org.ffs.aranya;

import android.annotation.SuppressLint;
import android.location.Location;
import android.support.v4.app.Fragment;

public class AranyaLazyInitializer<T extends AranyaLazyInitializer> extends Fragment {
   protected AranyaListener initializer;
   protected boolean isInitialized = false;
   
   public void setInitializer(AranyaListener initializer) {
      this.initializer = initializer;
   }
   
   public void initialize() {
      if(!isInitialized && this.initializer != null) {
         this.initializer.triggerCallback();
         this.isInitialized = true;
      }
   }
}
