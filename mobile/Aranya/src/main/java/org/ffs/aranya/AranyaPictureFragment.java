package org.ffs.aranya;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static org.ffs.aranya.AranyaStorageHelper.*;

public class AranyaPictureFragment extends Fragment {
   private ImageButton bCapture, bDelete;
   private ImageView imageView;
   private FrameLayout imageCapture;
   private CameraSurfaceView preview;
   private byte[] imageData;
   private byte[] currentPicture;
   private static final String TAG = "AranyaPic";
   private ViewGroup rootView;
   private boolean isCreated = false;
   
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      System.out.println("AranyaPictureFragment: in onCreate....");
      rootView = (ViewGroup) inflater.inflate(R.layout.add_pic, container, false);
   
      System.out.println("Initializing Aranya Picture Fragment...");
      imageView = rootView.findViewById(R.id.image_view);
      imageCapture = rootView.findViewById(R.id.image_capture);
      bCapture = rootView.findViewById(R.id.img_snap);
      bDelete = rootView.findViewById(R.id.img_del);
   
      Log.d(TAG, "Adding view for capturing images...");
      preview = new CameraSurfaceView(getActivity().getApplicationContext());
      imageCapture.addView(preview);
   
      bCapture.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            Log.d(TAG, "Capture button clicked...");
            if (preview.isCameraEnabled()) {
               preview.getCamera().takePicture(null, null, new HandlePictureStorage());
            }
         }
      });
      
      bDelete.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            deleteImageFile(AranyaMainActivity.getCurrentUuid());
            showCameraView();
         }
      });
   
      isCreated = true;
      Log.d(TAG, "AranyaPictureFragment: Showing camera view...");
      initializeForm();
      return rootView;
   }
   
   public void showPictureView() {
      Log.d(TAG, "Showing picture view...");
      if(isCreated) {
         imageCapture.setVisibility(View.GONE);
         imageView.setVisibility(View.VISIBLE);
         preview.stopPreview();
      }
   }
   
   public void showCameraView() {
      Log.d(TAG, "Showing camera view...");
      if(isCreated) {
         // Switch visibility so that the camera is hidden, and the image button shows..
         imageView.setVisibility(View.GONE);
         imageCapture.setVisibility(View.VISIBLE);
         preview.startPreview();
      }
   }
   
   public void initializeForm() {
      Log.d(TAG, "AranyaPictureFragment: initializing Form...");
      showCameraView();
   }
   
   // Handle an image capture event...
   private class HandlePictureStorage implements android.hardware.Camera.PictureCallback {
      
      @Override
      public void onPictureTaken(byte[] picture, Camera camera) {
         Log.i(TAG, "Picture successfully taken: " + picture.length + " bytes...");
         currentPicture = picture;
         
         java.io.File pictureFile = new java.io.File(getPictureStoragePath(AranyaMainActivity.getCurrentUuid()));
         if (pictureFile == null) {
            Log.d(TAG, "error creating media file, check storage permissions...");
         }
         
         try {
            Log.d(TAG, "Storing media file [" + pictureFile.getAbsolutePath() + "]");
            FileOutputStream pictureFileStream = new FileOutputStream(pictureFile);
            pictureFileStream.write(picture);
            pictureFileStream.close();
         } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
         } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
         }
         
         // Now change the display so that the image is shown on screen...
         final Bitmap bitmap = BitmapFactory.decodeByteArray(picture, 0, picture.length);
         imageView.setImageBitmap(bitmap);
         
         showPictureView();
      }
   }
   
   public boolean isCreated() {
      return isCreated;
   }
}
