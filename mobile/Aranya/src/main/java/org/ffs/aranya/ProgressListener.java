package org.ffs.aranya;

/**
 * Created by blaidddrwg on 1/10/18.
 */

public interface ProgressListener {
   public void updateProgress(int progressStatus);
}
