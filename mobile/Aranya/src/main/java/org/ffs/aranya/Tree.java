package org.ffs.aranya;

public class Tree {
   public enum Type {
      UUID, SPECIES, GBH, LON, LAT, ALT, NOTES;
   }
   
   private String uuid;
   private String species;
   private double gbh;
   private double lon;
   private double lat;
   private double alt;
   private String notes;
   
   public String getUuid() {
      return uuid;
   }
   
   public void setUuid(String uuid) {
      this.uuid = uuid;
   }
   
   public String getSpecies() {
      return species;
   }
   
   public void setSpecies(String species) {
      this.species = species;
   }
   
   public double getGbh() {
      return gbh;
   }
   
   public void setGbh(double gbh) {
      this.gbh = gbh;
   }
   
   public double getLon() {
      return lon;
   }
   
   public void setLon(double lon) {
      this.lon = lon;
   }
   
   public double getLat() {
      return lat;
   }
   
   public void setLat(double lat) {
      this.lat = lat;
   }
   
   public double getAlt() {
      return alt;
   }
   
   public void setAlt(double alt) {
      this.alt = alt;
   }
   
   public String getNotes() {
      return notes;
   }
   
   public void setNotes(String notes) {
      this.notes = notes;
   }
}
