package org.ffs.aranya;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;

public class AranyaStorageHelper {
   private static java.io.File storageDir;
   private static final String TAG = "AranyaStor";
   
   public AranyaStorageHelper(String appName) {
      storageDir = new java.io.File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), appName);
      initializeStorage();
   }
   @NonNull
   public static String getPictureFileName(String uuid) {
      return uuid + ".jpg";
   }
   
   @NonNull
   public static String getDataFileName(String uuid) {
      return uuid + ".dat";
   }
   
   @NonNull
   public static java.io.File getStoragePath() {
      return storageDir;
   }
   
   @NonNull
   public static String getPictureStoragePath(String uuid) {
      return storageDir + java.io.File.separator + getPictureFileName(uuid);
   }
   
   @NonNull
   public static String getDataStoragePath(String uuid) {
      return storageDir + java.io.File.separator + getDataFileName(uuid);
   }
   
   @NonNull
   public static java.io.File getStorageDir() {
      return storageDir;
   }
   
   private void initializeStorage() {
      if (!storageDir.exists()) {
         if (!storageDir.mkdirs()) {
            Log.d(TAG, "Failed to create application directory [" + storageDir.getAbsolutePath() + "]");
            return;
         }
      }
   }
   
   public static void deleteImageFile(String uuid) {
      File imageFile = new File(getPictureStoragePath(uuid));
      imageFile.delete();
   }
   
   public static void deleteDataFile(String uuid) {
      File dataFile = new File(getDataStoragePath(uuid));
      dataFile.delete();
   }
}
