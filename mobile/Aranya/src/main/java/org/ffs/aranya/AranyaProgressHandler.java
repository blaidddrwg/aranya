package org.ffs.aranya;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by blaidddrwg on 1/10/18.
 */

public class AranyaProgressHandler extends Handler {
   private final ProgressListener progressListener;
   private static final String TAG = "AranyaProgressHandler";
   
   public AranyaProgressHandler(Looper looper, ProgressListener progressListener) {
      super(looper);
      Log.d(TAG, "Creating AranyaProgressHandler...");
      this.progressListener = progressListener;
   }
   
   @Override
   public void handleMessage(Message msg) {
      Log.d(TAG, "Progress Listener [" + msg.arg1 + "]...");
      progressListener.updateProgress(msg.arg1);
   }
}
