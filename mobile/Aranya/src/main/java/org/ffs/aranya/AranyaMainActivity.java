package org.ffs.aranya;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import static org.ffs.aranya.AranyaStorageHelper.*;
import static org.ffs.aranya.AranyaDataSync.*;

public class AranyaMainActivity extends AppCompatActivity implements LocationListener, EasyPermissions.PermissionCallbacks {
   private LocationManager locationManager;
   public static final int NUM_PAGES = 2;
   
   private static final String[] REQUIRED_PERMS = {
         Manifest.permission.CAMERA,
         Manifest.permission.ACCESS_FINE_LOCATION,
         Manifest.permission_group.LOCATION,
         Manifest.permission_group.STORAGE,
         Manifest.permission.READ_EXTERNAL_STORAGE,
         Manifest.permission.WRITE_EXTERNAL_STORAGE,
   };
   
   static final int REQUEST_ACCOUNT_PICKER = 1000;
   static final int REQUEST_AUTHORIZATION = 1001;
   static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
   static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
   static final int REQUEST_READ_CONTACTS = 1004;
   
   private static final String PREF_ACCOUNT_NAME = "accountName";
   private static final String[] SCOPES = {DriveScopes.DRIVE,};
   private GoogleAccountCredential mCredential;
   private static final int PERMISSIONS_REQUEST = 1337;
   
   private static final String TAG = "AranyaMain";
   private static String currentUuid;
   private AranyaProgressHandler progressHandler;
   private final Bundle uuidBundle = new Bundle();
   
   // Constituent Fragments...
   private final AranyaFormFragment formFragment = new AranyaFormFragment();
   private final AranyaPictureFragment pictureFragment = new AranyaPictureFragment();
   private AranyaStorageHelper storageHelper;
   
   // Pager support
   private ViewPager mPager;
   private PagerAdapter mPagerAdapter;
   
   // Service binding structures..
   // Following the sample code from: https://stackoverflow.com/questions/4300291/example-communication-between-activity-and-service-using-messaging
   private Messenger mService = null;
   boolean mIsBound;
   final Messenger mMessenger = new Messenger(new IncomingHandler());
   
   class IncomingHandler extends Handler {
      @Override
      public void handleMessage(Message msg) {
         switch(msg.what) {
            case MSG_USER_REQUEST:
               try {
                  if (mCredential != null) {
                     Log.d(TAG, "Handling user request: " + mCredential.getSelectedAccountName());
                     Message userRequestResponseMessage = Message.obtain(null, MSG_USER_RESPONSE);
                     Bundle bundle = new Bundle();
                     bundle.putString("user", mCredential.getSelectedAccountName());
                     userRequestResponseMessage.setData(bundle);
                     Log.d(TAG, "Incoming Handler: Using service [" + mService + "] for communication...");
                     mService.send(userRequestResponseMessage);
                  }
               } catch (RemoteException e) {
                  e.printStackTrace();
               }
               break;
               
            case MSG_STATUS:
               int treesLeftToTransmit = msg.arg1;
               if(treesLeftToTransmit == 0) {
                  formFragment.setStatus("All caught up...");
               }
               else {
                  formFragment.setStatus("Trees left to transmit: " + treesLeftToTransmit);
               }
               break;
               
            default:
               super.handleMessage(msg);
               Log.d(TAG, "Total files pending transmission [" + msg.arg1 + "]");
         }
      }
   }
   
   private class AranyaServiceConnection implements ServiceConnection {
      public AranyaServiceConnection() {
         super();
         Log.d(TAG, "Creating AranyaServiceConnection...");
      }
      
      @Override
      public void onServiceDisconnected(ComponentName name) {
         try {
            Message msg = Message.obtain(null, AranyaDataSync.MSG_UNREGISTER_CLIENT);
            msg.replyTo = mMessenger;
            mService.send(msg);
         }
         catch(RemoteException e) {
            // do Nothing...
         }
         
         mService = null;
         // mCallbackText.setText("Disconnected.");
      }
   
      @Override
      public void onServiceConnected(ComponentName className, IBinder service) {
         Log.d(TAG, "AranyaServiceConnection: onServiceConnected called...");
         mService = new Messenger(service);
         Log.d(TAG, "mService is now set: " + mService);
         // mCallbackText.setText("Attached.");
      
         try {
            Message msg = Message.obtain(null, AranyaDataSync.MSG_REGISTER_CLIENT);
            msg.replyTo = mMessenger;
            
            mService.send(msg);
         }
         catch(RemoteException e) {
            // do Nothing...
         }
      }
   };
   private ServiceConnection mConnection = new AranyaServiceConnection();
   
   void doBindService() {
      Log.d(TAG, "onBindService called...");
      bindService(new Intent(AranyaMainActivity.this, AranyaDataSync.class), mConnection, Context.BIND_AUTO_CREATE);
      mIsBound = true;
      //mCallbackText.setText("Binding.");
   }
   
   void doUnbindService() {
      Log.d(TAG, "onUnbindService called...");
      if(mIsBound) {
         if(mService != null) {
            try {
               Message msg = Message.obtain(null, AranyaDataSync.MSG_UNREGISTER_CLIENT);
               msg.replyTo = mMessenger;
               mService.send(msg);
            }
            catch(RemoteException e) {
               // Nothing much to do here..
            }
         }
         
         unbindService(mConnection);
         mIsBound = false;
         // mCallbackText.setText("Unbinding.");
      }
   }
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setTitle(getResources().getString(R.string.title_activity_add));
      
      mCredential = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Arrays.asList(SCOPES)).setBackOff(new ExponentialBackOff());
      Log.d(TAG, "Created credentials for user [" + mCredential.getSelectedAccountName() + "]");
      storageHelper = new AranyaStorageHelper(getResources().getString(R.string.app_name));
      authenticateUser();
      checkIfServiceIsRunning();
   }
   
   @Override
   protected void onDestroy() {
      super.onDestroy();;
      try {
         doUnbindService();
      }
      catch(Throwable t) {
         Log.e(TAG, "Failed to unbind from the service");
      }
   }
   
   private void checkIfServiceIsRunning() {
      if(AranyaDataSync.isRunning()) {
         doBindService();
      }
   }
   
   @NonNull
   public static String getCurrentUuid() {
      return currentUuid;
   }
   
   private void authenticateUser() {
      Log.d(TAG, "Signing in to our overlord...");
      if (!isGooglePlayServicesAvailable()) {
         Log.d(TAG, "Google play service available.. acquiring...");
         acquireGooglePlayServices();
      }
      
      if (mCredential.getSelectedAccountName() == null) {
         chooseAccount();
      }
   
      showAranyaUserInterface(mCredential.getSelectedAccount());
   }
   
   private boolean isGooglePlayServicesAvailable() {
      Log.d(TAG, "Checking if Google Play Services are available...");
      GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
      final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
      return connectionStatusCode == ConnectionResult.SUCCESS;
   }
   
   private void acquireGooglePlayServices() {
      Log.d(TAG, "Acquiring Google Play Services...");
      GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
      final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
      if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
         showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
      }
   }
   
   @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
   private void chooseAccount() {
      Log.d(TAG, "Choosing account..");
      // If we have a 'REQUEST_PERMISSION_GET_ACCOUNTS' permission granted annotation for this function, what sense does this make ?
      if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS)) {
         String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
         Log.d(TAG, "Chose account name [" + accountName + "]");
         if (accountName != null) {
            mCredential.setSelectedAccountName(accountName);
         } else {
            Log.d(TAG, "Starting activity for logging in user...");
            startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
         }
      } else {
         EasyPermissions.requestPermissions(this, "This app needs to access your Google account (via Contacts).", REQUEST_PERMISSION_GET_ACCOUNTS, Manifest.permission.GET_ACCOUNTS);
      }
   }
   
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      
      switch (requestCode) {
         case REQUEST_GOOGLE_PLAY_SERVICES:
            Log.d(TAG, "Handling Activity result for REQUEST_GOOGLE_PLAY_SERVICE: " + resultCode);
            if (resultCode != RESULT_OK) {
               Toast.makeText(this, "This app requires Google Play Services. Please install " +
                     "Google Play Services on your device and relaunch this app.", Toast.LENGTH_LONG);
            }
            
            break;
         
         case REQUEST_ACCOUNT_PICKER:
            if (resultCode != RESULT_CANCELED && data != null && data.getExtras() != null) {
               String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
               Log.d(TAG, "Proceeding using account name [" + accountName + "]");
               if (accountName != null) {
                  SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
                  SharedPreferences.Editor editor = settings.edit();
                  editor.putString(PREF_ACCOUNT_NAME, accountName);
                  editor.apply();
                  mCredential.setSelectedAccountName(accountName);
               }
            }
            
            break;
         
         case REQUEST_AUTHORIZATION:
            Log.d(TAG, "Handling Activity Result for REQUEST_AUTHORIZATION: " + resultCode);
            if (resultCode != RESULT_OK) {
               Toast.makeText(this, "Authorization Request failed. Please authorize requested use in order to launch the app.", Toast.LENGTH_LONG);
            }
            
            break;
            
         case REQUEST_READ_CONTACTS:
            Log.d(TAG, "Handling Activity Result for REQUEST_READ_CONTACTS: " + resultCode);
            break;
      }
   
      chooseAccount();
   }
   
   private void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
      GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
      Dialog dialog = apiAvailability.getErrorDialog(
            AranyaMainActivity.this, connectionStatusCode, REQUEST_GOOGLE_PLAY_SERVICES);
      
      dialog.show();
   }
   
   @Override
   public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
   }
   
   @Override
   public void onPermissionsGranted(int requestCode, List<String> list) {
      // Do nothing...
   }
   
   @Override
   public void onPermissionsDenied(int requestCode, List<String> list) {
      // Do nothing...
   }
   
   private boolean isDeviceOnline() {
      ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
      return (networkInfo != null && networkInfo.isConnected());
   }
   
   private void showAranyaUserInterface(Account account) {
      if (!permissionGrantedForRelevantSystemServices()) return;
   
      verifyLocationAccessPermitted();
      verifyCameraAccessPermitted();
   
      setContentView(R.layout.activity_add);
      Toolbar toolbar = findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      doBindService();
      
      Log.d(TAG, "Initializing form for the first time...");
      initializeUuid();
      mPager = (ViewPager) findViewById(R.id.pager);
      mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager()) {
      
      };
      
      Log.d(TAG, "Adding page listener for pager component...");
      mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
         @Override
         public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
         }
   
         @Override
         public void onPageSelected(int position) {
            Log.d(TAG, "Page selected: [" + position + "]");
            Log.d(TAG, "Using service connection: " + mService);
            if(position == 1) hideKeyboardIfPresent();
         }
   
         @Override
         public void onPageScrollStateChanged(int state) {
         }
      });
      
      Log.d(TAG, "Adding pager adapter...");
      mPager.setAdapter(mPagerAdapter);
      
      Log.d(TAG, "Service connection in use is [" + mService + "]");
      formFragment.addActionListener(() -> {
         Log.d(TAG, "Callback triggered for 'save' button press...");
         saveFormData(AranyaMainActivity.this.mService);
      });
   }
   
   private void initializeForm() {
      Log.d(TAG, "Calling initializeForm()...");
      initializeUuid();
      if(pictureFragment.isCreated()) pictureFragment.initializeForm();
      if(formFragment.isCreated()) formFragment.initializeForm();
      doBindService();
   }
   
   private void initializeUuid() {
      currentUuid = UUID.randomUUID().toString();
   }
   
   private void hideKeyboardIfPresent() {
      Log.d(TAG, "Checking if we should hide the keyboard...");
      if(getCurrentFocus() != null) {
         Log.d(TAG, "Valid context - we should hide the keyboard...");
         InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
         imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
      }
   }
   
   private void verifyCameraAccessPermitted() {
      if (canAccessCameraService()) {
         Log.d(TAG, "Checking if camera service can be accessed: yes");
      } else {
         // Todo: What happens if the camera service has no permissions ? We should really exit...
         Log.d(TAG, "Cannot proceed without access to the camera.");
         final Toast needCameraError = Toast.makeText(this.getApplicationContext(), "Need access to Camera, or we are useless !", Toast.LENGTH_LONG);
         needCameraError.show();
         
         System.exit(0);
      }
   }
   
   private void verifyLocationAccessPermitted() {
      if (canAccessLocation()) {
         locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
      
         if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
               ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
               !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(TAG, "Cannot proceed without access to the location.");
            final Toast needLocationError = Toast.makeText(this.getApplicationContext(), "Need access to Location, or we are useless !", Toast.LENGTH_LONG);
            needLocationError.show();
   
            System.exit(0);
         }
         
         locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
      }
   }
   
   // Who calls this ? Why is it here ?
   private void handleServiceError(Exception mLastError) {
      if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
         showGooglePlayServicesAvailabilityErrorDialog(((GooglePlayServicesAvailabilityIOException) mLastError).getConnectionStatusCode());
      } else if (mLastError instanceof UserRecoverableAuthIOException) {
         startActivityForResult(((UserRecoverableAuthIOException) mLastError).getIntent(), AranyaMainActivity.REQUEST_AUTHORIZATION);
      }
      
      authenticateUser();
   }
   
   private void saveFormData(Messenger serviceConnection) {
      Log.d(TAG, "Saving form data, is mService null ?: " + (serviceConnection == null));
      if(formFragment.hasValidData()) {
         Message msg = Message.obtain(null, AranyaDataSync.MSG_SAVE_DATA);
         uuidBundle.putString("uuid", currentUuid);
         msg.setData(uuidBundle);
         try {
            PrintStream dataFileWriter = new PrintStream(new java.io.File(getDataStoragePath(currentUuid)));
            dataFileWriter.print(formFragment.getSerializedData());
            dataFileWriter.close();
   
            serviceConnection.send(msg);
            initializeForm();
         } catch (RemoteException e) {
            e.printStackTrace();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
      else {
         formFragment.setStatus("Need valid fields for GBH and lat/long/alt...");
      }
   }

   @Override
   protected void onStart() {
      super.onStart();
      //doBindService();
   }
   
   @Override
   protected void onStop() {
      super.onStop();
      doUnbindService();
   }
   
   @Override
   protected void onPause() {
      super.onPause();
   }
   
   private boolean permissionGrantedForRelevantSystemServices() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
         requestPermissions(REQUIRED_PERMS, PERMISSIONS_REQUEST);
      }
      
      return true;
   }
   
   private boolean canAccessLocation() {
      return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
   }
   
   private boolean canAccessCameraService() {
      return (hasPermission(Manifest.permission.CAMERA));
   }
   
   private boolean hasPermission(String perm) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
         return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
      }
      
      return true;
   }
   
   @Override
   public void onLocationChanged(Location location) {
      formFragment.onLocationChanged(location);
   }
   
   @Override
   public void onStatusChanged(String provider, int status, Bundle extras) {
      //Todo: Have to figure out what to do if the status is changed...
   }
   
   @Override
   public void onProviderEnabled(String provider) {
      Toast.makeText(this, "Enabled new provider " + provider, Toast.LENGTH_SHORT).show();
   }
   
   @Override
   public void onProviderDisabled(String provider) {
      Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_SHORT).show();
   }
   
   
   @Override
   public void onBackPressed() {
      if(mPager.getCurrentItem() == 0) {
         super.onBackPressed();
      }
      else {
         mPager.setCurrentItem(mPager.getCurrentItem() - 1);
      }
   }
   
   private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
      public ScreenSlidePagerAdapter(FragmentManager supportFragmentManager) {
         super(supportFragmentManager);
      }
      
      @Override
      public Fragment getItem(int position) {
         System.out.println("ScreenSlidePageAdapter called: position = " + position);
         if(position == 0) {
            System.out.println("ScreenSlidePagerAdapter: Creating new fragment : AranyaFormFragment...");
            return AranyaMainActivity.this.formFragment;
         }
   
         System.out.println("ScreenSlidePagerAdapter: Creating new fragment : AranyaPictureFragment...");
         return AranyaMainActivity.this.pictureFragment;
      }
      
      @Override
      public int getCount() {
         return NUM_PAGES;
      }
   }
}
