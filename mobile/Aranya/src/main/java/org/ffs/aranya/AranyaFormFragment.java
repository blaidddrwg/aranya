package org.ffs.aranya;

import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AranyaFormFragment extends Fragment implements ProgressListener, AranyaTrigger {
   private Spinner speciesDropDown;
   private TextView mLongitude;
   private TextView mLatitude;
   private TextView mAltitude;
   private TextView mGBH;
   private TextView mNotes;
   private ImageButton bCoord;
   private ProgressBar progressBar;
   private TextView mStatus;
   
   private double longitude;
   private double latitude;
   private double altitude;
   
   private static final String TAG = "AranyaPic";
   private ViewGroup rootView;
   boolean isCreated = false;
   
   DecimalFormat formatLatLon = new DecimalFormat("###.######");
   DecimalFormat formatAltitude = new DecimalFormat("######.#");
   
   private final List<AranyaListener> listeners = new ArrayList<AranyaListener>();

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      System.out.println("AranyaFormFragment: in onCreate....");
      rootView = (ViewGroup) inflater.inflate(R.layout.add_data, container, false);
   
      System.out.println("Initializing Aranya Form Fragment...");
      progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
      bCoord = (ImageButton) rootView.findViewById(R.id.coords);
      speciesDropDown = (Spinner) rootView.findViewById(R.id.species_spinner);
      mLongitude = rootView.findViewById(R.id.longitude);
      mLatitude = rootView.findViewById(R.id.latitude);
      mAltitude = rootView.findViewById(R.id.altitude);
      mGBH = rootView.findViewById(R.id.gbh);
      mNotes = rootView.findViewById(R.id.notes);
      mStatus = rootView.findViewById(R.id.status);
   
      ArrayAdapter<CharSequence> speciesAdapter = ArrayAdapter.createFromResource(this.getContext(), R.array.species_array, android.R.layout.simple_spinner_item);
      speciesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      speciesDropDown.setAdapter(speciesAdapter);
   
      bCoord.setOnClickListener(v -> {
         mLongitude.setText(formatLatLon.format(longitude));
         mLatitude.setText(formatLatLon.format(latitude));
         mAltitude.setText(formatAltitude.format(altitude));
      });
   
      FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
      fab.setOnClickListener(view -> {
         for(AranyaListener listener : listeners) {
            listener.triggerCallback();
         }
      });
      
      isCreated = true;
      initializeForm();
      return rootView;
   }
   
   public boolean hasValidData() {
      if(mLongitude.getText().length() > 0 &&
            mLatitude.getText().length() > 0 &&
            mAltitude.getText().length() > 0 &&
            mGBH.getText().length() > 0) return true;
      
      return false;
   }
   
   public void initializeForm() {
      Log.d(TAG, "AranyaFormFragment: initializing form...");
      if(isCreated) {
         mLongitude.setText("");
         mLatitude.setText("");
         mAltitude.setText("");
         mGBH.setText("");
         mNotes.setText("");
   
         progressBar.setProgress(0);
         progressBar.setVisibility(View.GONE);
   
         // Get ready for new data entry. Keep the species selected, in case we have a number of trees
         // of the same species...
         mGBH.requestFocus();
      }
   }
   
   public void onLocationChanged(Location location) {
      this.longitude = location.getLongitude();
      this.latitude = location.getLatitude();
      this.altitude = location.getAltitude();
   }
   
   public ProgressBar getProgressBar() {
      return progressBar;
   }
   
   public boolean isCreated() {
      return isCreated;
   }
   
   public void updateProgress(int progressStatus) {
      Log.d(TAG, "Progress Listener [" + progressStatus + "]...");
      if (progressStatus == 100) {
         progressBar.setVisibility(View.GONE);
      } else {
         if (progressBar.getVisibility() != View.VISIBLE) progressBar.setVisibility(View.VISIBLE);
         System.out.println("Progress bar: setting to [" + progressStatus + "]");
         progressBar.setProgress(progressStatus);
      }
   }
   
   @Override
   public void addActionListener(AranyaListener listener) {
      listeners.add(listener);
   }
   
   public String getSerializedData() {
      StringBuffer sbData = new StringBuffer();
      sbData.append("SPECIES=").append(speciesDropDown.getSelectedItem().toString()).append("\n")
            .append("GBH=").append(mGBH.getText().toString()).append("\n")
            .append("LON=").append(mLongitude.getText().toString()).append("\n")
            .append("LAT=").append(mLatitude.getText().toString()).append("\n")
            .append("ALT=").append(mAltitude.getText().toString()).append("\n")
            .append("NOTES=").append(mNotes.getText().toString()).append("\n");
   
      return sbData.toString();
   }
   
   public void setStatus(String status) {
      mStatus.setText(status);
   }
}
