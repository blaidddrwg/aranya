package org.ffs.aranya;

/**
 * Copyright (c) {2003,2011} {openmobster@gmail.com} {individual contributors as indicated by the @authors tag}.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.IOException;
import java.util.List;

public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
   private static final String TAG = "CameraSurfaceView";
   private final SurfaceHolder holder;
   private final Context context;
   private Camera camera;
   private static int cameraId = 0;
   private Camera.Parameters cameraParam;
   private static android.hardware.Camera.CameraInfo info;
   private static boolean cameraEnabled = false;
   
   public CameraSurfaceView(Context context) {
      super(context);
      this.context = context;
      
      info = new android.hardware.Camera.CameraInfo();
      
      this.holder = this.getHolder();
      this.holder.addCallback(this);
      printCameraHardwareInfo();
   
      try {
         this.camera = getCameraInstance();
         this.camera.setPreviewDisplay(holder);
      }
      catch(IOException ioe) {
         ioe.printStackTrace();
         Log.d(TAG, "Could not set camera preview display to holder...");
      }
   }
   
   public void printCameraHardwareInfo() {
      final int cameraCount = Camera.getNumberOfCameras();
      Log.i(TAG, "Found " + cameraCount + " cameras..");

      for (int iCamera = 0; iCamera < cameraCount; iCamera++) {
         final CameraInfo cameraInfo = new CameraInfo();
         Camera.getCameraInfo(iCamera, cameraInfo);
         Log.i(TAG, "Camera[" + iCamera + "]: " + cameraInfo.toString());
      }
   }
   
   public boolean isCameraEnabled() {
      return cameraEnabled;
   }
   
   public static Camera getCameraInstance()  {
      Log.d(TAG, "getCameraInstance called...");
      
      Camera camera = Camera.open();
      while (camera == null) {
         Log.d(TAG, "Trying to open camera...");
         camera = Camera.open(cameraId);
      }
      
      return camera;
   }
   
   @Override
   public void surfaceCreated(SurfaceHolder holder) {
      Log.d(TAG, "surfaceCreated called...");
      if(camera != null) {
         try {
            camera.setErrorCallback(CEC);
            camera.setDisplayOrientation(0);
            startPreview();
         }
         catch(Exception e) {
            Log.d(TAG, e.getMessage());
         }
      }
   }
   
   public void startPreview() {
      Log.d(TAG, "startPreview called...");
   
      setCameraOrientation(holder);
      cameraEnabled = true;
      camera.startPreview();
   }
   
   
   public void stopPreview() {
      Log.d(TAG, "stopPreview called...");
      
      Log.d(TAG, "Camera is not null - starting preview...");
      camera.stopPreview();
      cameraEnabled = false;
   }
   
   private void releaseCameraResources() {
      Log.d(TAG, "Releasing camera resources...");
      
      if(camera != null) {
         camera.stopPreview();
         camera.release();
      }
      
      camera = null;
   }
   
   @Override
   public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
      Log.d(TAG, "handling surfaceChanged event...");
   
      if(holder.getSurface() == null) {
         return;
      }

      Log.d(TAG, "Previewing: camera is enabled [" + cameraEnabled + "]");
      if(camera != null) {
         releaseCameraResources();
         cameraEnabled = false;
      }
   
   
      camera = getCameraInstance();
      setCameraOrientation(holder);
   
      camera.startPreview();
      cameraEnabled = true;
   }
   
   private void setCameraOrientation(SurfaceHolder holder) {
      try {
         Camera.getCameraInfo(cameraId, info);
         Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
         int degrees = 0;
         switch(display.getRotation()) {
            case Surface.ROTATION_0  : degrees = 180;   break;
            case Surface.ROTATION_90 : degrees = 270;  break;
            case Surface.ROTATION_180: degrees = 0; break;
            case Surface.ROTATION_270: degrees = 0; break;
         }
         
         int resultOrientation = 0;
         if(info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            resultOrientation = (info.orientation + degrees)%360;
            resultOrientation = (360 - resultOrientation)%360;
         }
         else {
            resultOrientation = (info.orientation - degrees + 360)%360;
         }
         
         camera.setDisplayOrientation(resultOrientation);
         
         Log.d(TAG, "Setting camera orientation to [" + info.orientation + "]...");
         int cameraRotationOffset = info.orientation;
   
         this.camera.setPreviewDisplay(holder);
      }
      catch(IOException ioe) {
         ioe.printStackTrace();
         Log.d(TAG, "Could not set preview display...");
      }
   }
   
   @Override
   public void surfaceDestroyed(SurfaceHolder holder) {
      Log.d(TAG, "handling surfaceDestroyed event...");
   
      releaseCameraResources();
   }
   
   public Camera getCamera() {
      return this.camera;
   }
   
   ErrorCallback CEC = new ErrorCallback() {
      @Override
      public void onError(int error, Camera camera) {
         Log.d(TAG, "camera error detected: " + error);
         if (error == Camera.CAMERA_ERROR_SERVER_DIED) {
            Log.d(TAG, "attempting to reinstantiate new camera");
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release(); // written in documentation...
            camera = null;
            camera = Camera.open();
         }
      }
   };
}
