package org.ffs.aranya;

import android.support.v4.app.Fragment;

interface AranyaListener<T extends AranyaLazyInitializer> {
   public void triggerCallback();
}
