package org.ffs.aranya;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import static org.ffs.aranya.AranyaStorageHelper.*;

public class AranyaDataSync extends Service implements ProgressListener {
   NotificationManager mNM;
   ArrayList<Messenger> mClients = new ArrayList<Messenger>();
   private FileUploadProgressListener fileUploadProgressListener;
   private final List<String> trees = new ArrayList<String>();
   private boolean transmissionInProgress = false;
   private AranyaStorageHelper storageHelper;
   public static boolean isRunning = false;
   
   public static final int KEY = 0;
   public static final int VAL = 1;
   private static final String[] SCOPES = {DriveScopes.DRIVE,};
   
   public static final String FAILURE = "0", SUCCESS = "1";
   static final int MSG_REGISTER_CLIENT = 1;
   static final int MSG_UNREGISTER_CLIENT = 2;
   static final int MSG_SAVE_DATA = 3;
   static final int MSG_STATUS = 4;
   static final int MSG_ERROR = 5;
   static final int MSG_USER_REQUEST = 6;
   static final int MSG_USER_RESPONSE = 7;
   
   private static final String TAG = "AranyaSvc";
   private static final String uploadDirId = "0B-HAAMnyL3nrflc4OERJMkVPWDNoSDN6OV9pRl9NbUNEMHBCcFhfVnJUUlZpOTZ5NFlFNlk";
   private static final String formId = "1FAIpQLSdEXdeWXK5ZCbtLkIFKX8isSxq-BaBM3xSgVu56hoZdjIWQvQ";
   private static GoogleAccountCredential mCredential = null;
   private static String userName;
   
   @Override
   public void onCreate() {
      storageHelper = new AranyaStorageHelper(getResources().getString(R.string.app_name));
      mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

      final Looper looper = Looper.getMainLooper();
      fileUploadProgressListener = new FileUploadProgressListener(new AranyaProgressHandler(looper, this));
   
      initializeFilesForTransmissionFromStorage();
      showNotification();
   
      isRunning = true;
      new Thread() {
         public void run() {
            updateStatusContinuously();
         }
      }.start();
      
      return;
   }
   
   public static boolean isRunning() {
      return isRunning;
   }
   
   private void initializeFilesForTransmissionFromStorage() {
      File[] dataFiles = storageHelper.getStorageDir().listFiles(new FilenameFilter() {
         @Override
         public boolean accept(File dir, String name) {
            return name.endsWith(".dat");
         }
      });
      
      for(File dataFile : dataFiles) {
         final String fileName = dataFile.getName();
         final int extensionStartIndex = fileName.indexOf(".");
         String uuid = extensionStartIndex > 0 ? fileName.substring(0, fileName.indexOf(".")) : fileName;
         trees.add(uuid);
      }
      
      sendMessageToMainActivity(Message.obtain(null, MSG_STATUS, trees.size(), 0));
   }
   
   public void updateStatusContinuously() {
      while(true) {
         try {
            Thread.sleep(10000);
         } catch (InterruptedException e) {
            e.printStackTrace();
         }

         if(mCredential == null) {
            Log.d(TAG, "Initializing SyncService credentials...");
            mCredential = GoogleAccountCredential.usingOAuth2(getApplicationContext(), Arrays.asList(SCOPES)).setBackOff(new ExponentialBackOff());
            Message requestUserName = Message.obtain(null, MSG_USER_REQUEST);
            sendMessageToMainActivity(requestUserName);
         }
         
         if(mCredential.getSelectedAccountName() == null && userName != null) {
            mCredential.setSelectedAccountName(userName);
            Log.d(TAG, "Using credentials for: " + mCredential.getSelectedAccountName());
         }
         
         if(mCredential != null && userName != null && trees.size() > 0) {
            SaveData uploader = new SaveData(mCredential);
            uploader.execute(trees.get(0));
         }
      }
   }
   
   @Override
   public void updateProgress(int progressStatus) {
      // Transmit the file upload status to the user interface...
   }
   
   class IncomingHandler extends Handler {
      @Override
      public void handleMessage(Message msg) {
         switch(msg.what) {
            case MSG_REGISTER_CLIENT:
               Log.d(TAG, "Getting credentials for user: " + msg.getData().getString("user"));
               mClients.add(msg.replyTo);
               break;
               
            case MSG_UNREGISTER_CLIENT:
               mClients.remove(msg.replyTo);
               break;
               
            case MSG_SAVE_DATA:
               final String uuid = msg.getData().get("uuid").toString();
               Log.d(TAG, "Got message [" + msg.what + "]: {" + uuid + "}");
               trees.add(uuid);
               sendMessageToMainActivity(Message.obtain(null, MSG_STATUS, trees.size(), 0));
   
               break;
               
            case MSG_USER_RESPONSE:
               userName = msg.getData().get("user").toString();
               Log.d(TAG, "Got user [" + userName + "]");
               break;
               
            default:
               Log.d(TAG, "Got message [" + msg.what + "]");
         }
      }
   }
   
   private Tree parseTree(String uuid) {
      Tree tree = new Tree();
      tree.setUuid(uuid);
      String read = null;
      
      try {
         final String dataFilePath = getDataStoragePath(uuid);
         final File dataFile = new File(dataFilePath);
         BufferedReader dataReader = new BufferedReader(new FileReader(dataFile));
         while ((read = dataReader.readLine()) != null) {
            String[] keyval = read.split("=");
            final String key = keyval[KEY];
            final String val = keyval[VAL];
            
            switch(Tree.Type.valueOf(key)) {
               case SPECIES:
                  tree.setSpecies(val);
                  break;
                  
               case GBH:
                  tree.setGbh(Double.parseDouble(val));
                  break;
                  
               case LAT:
                  tree.setLat(Double.parseDouble(val));
                  break;
                  
               case LON:
                  tree.setLon(Double.parseDouble(val));
                  break;
                  
               case ALT:
                  tree.setAlt(Double.parseDouble(val));
                  break;
                  
               case NOTES:
                  tree.setNotes(val);
                  break;
            }
         }
      }
      catch(IOException ioe) {
         Log.d(TAG, "Unable to parse file [" + uuid + ".dat");
         ioe.printStackTrace();
      }
      
      return tree;
   }
   
   final Messenger mMessenger = new Messenger(new IncomingHandler());
   
   @Override
   public void onDestroy() {
      mNM.cancel(R.string.remote_service_started);
      //Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
      System.out.println("AranyaDataSync: Remote Service stopped...");
   }
   
   private void showNotification() {
      //Toast.makeText(this, R.string.remote_service_running, Toast.LENGTH_SHORT).show();
      System.out.println("AranyaDataSync: Remote Service running...");
   }

   @Override
   public IBinder onBind(Intent intent) {
      return mMessenger.getBinder();
   }
   
   private void sendMessageToMainActivity(Message message) {
      for(int clientIndex = 0; clientIndex < mClients.size(); clientIndex++) {
         final Messenger client = mClients.get(clientIndex);
         Log.d(TAG, "Trying to send message to client [" + clientIndex + "]: " + (client == null));
   
         if (client != null) {
            try {
               client.send(message);
            } catch (RemoteException e) {
               e.printStackTrace();
            }
         }
      }
   }
   
   class SaveData extends AsyncTask<String, Integer, String> {
      private com.google.api.services.drive.Drive mService = null;
      private Exception mLastError = null;
      private MediaHttpUploader uploader;
      private String uuid;
      
      SaveData(GoogleAccountCredential credential) {
         HttpTransport transport = AndroidHttp.newCompatibleTransport();
         JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
         mService = new Drive.Builder(transport, jsonFactory, credential).setApplicationName("AranyaShare").build();
      }
      
      @Override
      protected String doInBackground(String... params) {
         transmissionInProgress = true;
         uuid = params[0];
         Tree tree = parseTree(uuid);
         if (savePicture() && saveFormData(tree).equals(null)) {
            return SUCCESS;
         }
         
         return FAILURE;
      }
      
      @NonNull
      private String saveFormData(Tree tree) {
         Log.d(TAG + ": SAVE_FORM", "Saving form data...");
         try {
            String googleFormsUrl = "https://docs.google.com/forms/d/e/" + formId + "/formResponse";
            URL googleFormSubmitUrl = new URL(googleFormsUrl);
            Uri.Builder builder = new Uri.Builder()
                  .appendQueryParameter("entry.769143525", tree.getUuid())
                  .appendQueryParameter("entry.1224634336", tree.getSpecies())
                  .appendQueryParameter("entry.1121084628", Double.toString(tree.getLon()))
                  .appendQueryParameter("entry.1972629943", Double.toString(tree.getLat()))
                  .appendQueryParameter("entry.1458489709", Double.toString(tree.getAlt()))
                  .appendQueryParameter("entry.1821471730", Double.toString(tree.getGbh()))
                  .appendQueryParameter("entry.2037439054", tree.getNotes());
            
            HttpURLConnection connection = (HttpsURLConnection) googleFormSubmitUrl.openConnection();
            
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            
            //String query = builder.build().getEncodedQuery();
            Log.d(TAG, "Establishing connection...");
            //connection.connect();
            
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(builder.build().getEncodedQuery());
            writer.flush();
            writer.close();
            Log.d(TAG, "Connection closed...");
            
            int responseCode = connection.getResponseCode();
            Log.d(TAG, "Got response code [" + responseCode + "]");
            
            InputStream is = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = reader.readLine()) != null) {
               Log.d(TAG, "Got line: " + line);
               response.append(line).append('\r');
            }
            reader.close();
            
            Log.d(TAG, "Reader closed...");
            return responseCode + "";
         } catch (IOException e) {
            Log.d(TAG, "Got exception trying to send data", e);
            e.printStackTrace();
            mLastError = e;
            cancel(true);
            return null;
         }
      }
   
      /**
       * Right now, this is failing (used to work before) with the error:
       * 1-14 03:54:23.221  1636  1727 D AranyaSvc: Error trying to upload file
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential$RequestHandler.intercept(GoogleAccountCredential.java:297)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.http.HttpRequest.execute(HttpRequest.java:868)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.media.MediaHttpUploader.executeCurrentRequestWithoutGZip(MediaHttpUploader.java:545)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.media.MediaHttpUploader.executeCurrentRequest(MediaHttpUploader.java:562)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.media.MediaHttpUploader.directUpload(MediaHttpUploader.java:360)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.media.MediaHttpUploader.upload(MediaHttpUploader.java:334)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.services.AbstractGoogleClientRequest.executeUnparsed(AbstractGoogleClientRequest.java:427)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.services.AbstractGoogleClientRequest.executeUnparsed(AbstractGoogleClientRequest.java:352)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.services.AbstractGoogleClientRequest.execute(AbstractGoogleClientRequest.java:469)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at org.ffs.aranya.AranyaDataSync$SaveData.savePicture(AranyaDataSync.java:364)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at org.ffs.aranya.AranyaDataSync$SaveData.doInBackground(AranyaDataSync.java:277)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at org.ffs.aranya.AranyaDataSync$SaveData.doInBackground(AranyaDataSync.java:260)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at android.os.AsyncTask$2.call(AsyncTask.java:305)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at java.util.concurrent.FutureTask.run(FutureTask.java:237)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at android.os.AsyncTask$SerialExecutor$1.run(AsyncTask.java:243)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1133)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:607)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at java.lang.Thread.run(Thread.java:761)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: Caused by: com.google.android.gms.auth.UserRecoverableAuthException: NeedPermission
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zze.zzac(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zzd.zza(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zzd.zzb(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zzd.getToken(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zzd.getToken(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.zzd.getToken(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.android.gms.auth.GoogleAuthUtil.getToken(Unknown Source)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential.getToken(GoogleAccountCredential.java:267)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	at com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential$RequestHandler.intercept(GoogleAccountCredential.java:292)
       * 11-14 03:54:23.221  1636  1727 D AranyaSvc: 	... 17 more
       *
       * Need to resolve this - links I was browsing include:
       *     https://developers.google.com/drive/api/v3/migration
       *     https://stackoverflow.com/questions/35143283/google-drive-api-v3-migration
       *
       * Maybe also check out some of the points mentioned here:
       *     https://stackoverflow.com/questions/17428066/no-intent-from-userrecoverableauthioexception-using-drive-sdk-for-android
       *
       * Main question is - why are we using Drive v3 API, instead of native Android upload functionality (since this is an app ?) I know there was a reason,
       * but I'm not able to remember it. Perhaps, it was because we couln't set the parent ? If so, we should look at this:
       *
       *      https://developers.google.com/drive/android/create-file
       *
       * Which seems to have stuff for setting the parent
       *
       * @return
       */
      private boolean savePicture() {
         Log.d(TAG + ": SAVE_PICT", "Saving form data...");
         java.io.File filePath = new java.io.File(getPictureStoragePath(uuid));
         Log.d(TAG, "Sending file {" + getPictureStoragePath(uuid) + "} with size [" + filePath.length() + "]");
   
         if (filePath.exists() /*because all tree entries may not have a photograph*/) {
            try {
               Log.d(TAG, "Uploading file [" + uuid + ".jpg" + "]");
               com.google.api.services.drive.model.File fileMetaData = new com.google.api.services.drive.model.File();
               fileMetaData.setName(getPictureFileName(uuid));
               fileMetaData.setParents(Collections.singletonList(uploadDirId));
               Log.d(TAG, "Set parent to [" + uploadDirId + "] :" + fileMetaData);
         
               FileContent mediaContent = new FileContent("image/jpeg", filePath);
               Log.d(TAG, "Created file content [" + mediaContent.getLength() + "] bytes...");
         
               final Drive.Files.Create create = mService.files().create(fileMetaData, mediaContent);
               uploader = create.getMediaHttpUploader();
               uploader.setDirectUploadEnabled(true);
         
               //Log.d(TAG, "Adding progress listener to image upload...");
               //uploader.setProgressListener(AranyaMainActivity.this.fileUploadProgressListener);
         
               com.google.api.services.drive.model.File file = create.setFields("id, parents").execute();
               Log.d(TAG, "Created image file [" + file.getId() + "]");
            } catch (UserRecoverableAuthIOException e) {
               startActivity(e.getIntent());
            } catch (IOException e) {
               Log.d(TAG, "Error trying to upload file", e);
               e.printStackTrace();
               mLastError = e;
               cancel(true);
               return false;
            }
         }
         
         Log.d(TAG, "\n");
         return true;
      }
   
      @Override
      protected void onProgressUpdate(Integer... progress) {
      }
   
      @Override
      protected void onPostExecute(String result) {
         transmissionInProgress = false;
         removeTransmittedFilesFromStorage(uuid);
      }
   
      private void removeTransmittedFilesFromStorage(String uuid) {
         storageHelper.deleteImageFile(uuid);
         storageHelper.deleteDataFile(uuid);
         
         trees.remove(uuid);
         sendMessageToMainActivity(Message.obtain(null, MSG_STATUS, trees.size(), 0));
      }
      
      @Override
      protected void onCancelled() {
         if (mLastError != null) {
            sendMessageToMainActivity(Message.obtain(null, MSG_ERROR, mLastError));
         }
      }
   }
}
