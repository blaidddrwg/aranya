package org.ffs.aranya;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;

import java.io.IOException;

/**
 * Created by blaidddrwg on 1/9/18.
 * With grateful thanks from https://stackoverflow.com/questions/13580109/check-progress-for-upload-download-google-drive-api-for-android-or-java
 */

public class FileUploadProgressListener implements MediaHttpUploaderProgressListener {
   private final Handler uploadHandler;
   private static final String TAG = "FUProgressListener";
   
   public FileUploadProgressListener(Handler handler) {
      Log.d(TAG, "Creating FileUploadProgressListener...");
      this.uploadHandler = handler;
   }
   
   @Override
   public void progressChanged(MediaHttpUploader uploader) throws IOException {
      if(uploader == null) return;
      
      switch(uploader.getUploadState()) {
         case INITIATION_STARTED:
            break;
            
         case INITIATION_COMPLETE:
            break;
            
         case MEDIA_IN_PROGRESS: {
            final Message message = uploadHandler.obtainMessage();
            message.arg1 = (int) (uploader.getProgress() * 100);
            Log.d(TAG, "MEDIA_IN_PROGRESS [" + message.arg1 + "]...");
            uploadHandler.sendMessage(message); }
            break;
            
         case MEDIA_COMPLETE: {
            final Message message = uploadHandler.obtainMessage();
            message.arg1 = 100;
            uploadHandler.sendMessage(message); }
            break;
      }
   }
}
