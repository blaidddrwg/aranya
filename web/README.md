#Aranya Open Source Project
---

Documenting species diversity in the field requires information collection in the field, and this app helps to ease the data collection process.

GPS integration allows the tree location (up to the limits of GPS accuracy) to be collected, along with other essential data like tree species, the "GBH" (Girth at Breast Height) and other information that can help track diversity changes over time. It also allows you to take a photograph of the tree for reference.

This initial build uploads all form data to a Google Spreadsheet, and the collected images to a shared Google drive.
