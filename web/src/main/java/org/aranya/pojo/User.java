package org.aranya.pojo;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Entity
@Table(name = "users")
@SuppressWarnings("serial")
public class User implements Serializable {

   @Id
   @GeneratedValue
   @Column(name = "id")
   private int id;

   @Column(name = "logn")
   String username;

   @Column(name = "pass")
   String password;

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }
}

// All fundaes from: https://www.roseindia.net/spring/spring4/login-form-using-spring-mvc-and-hibernate.shtml
