package org.aranya;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.aranya.pojo.User;

@SpringBootApplication
@ImportResource({ "classpath:applicationContext.xml" })
public class App {

   @Autowired
   SessionFactory sessionFactory;

   private static App app = null;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(App.class, args);
        System.out.println("These are the beans provided by Spring Boot:");
        app.showBeans(ctx);
    }

    public App() {
       if(app == null) app = this;
    }

    public void showBeans(ApplicationContext ctx) {
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }

        try {
           Session currentSession = sessionFactory.openSession();

           Transaction transaction = currentSession.getTransaction();
           transaction.begin();
           currentSession.createCriteria(User.class).list().stream().forEach(System.out::println);
           transaction.commit();
        }
        catch(Exception e) {
           e.printStackTrace();
        }
     }
}
