package org.aranya.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

import org.aranya.pojo.User;

public class UserDao implements GenericDao<User> {
   private SessionFactory sessionFactory;
   private String tenant;

   public UserDao(SessionFactory sessionFactory, String tenant) {
      this.sessionFactory = sessionFactory;
      this.tenant = tenant;
      populate();
   }

   @Override
   public void save(User entity) {
      Session session = sessionFactory.withOptions().tenantIdentifier(tenant).openSession();
      session.save(entity);
   }

   @Override
   public void delete(User user) {
      Session session = sessionFactory.withOptions().tenantIdentifier(tenant).openSession();
      session.delete(user);
   }

   @Override
   public User findByName(String name) {
      Session session = sessionFactory.withOptions().tenantIdentifier(tenant).openSession();
      List<User> users = session.createCriteria(User.class).add(Expression.eq("name", name)).list();
      if(users.size() > 0) {
         return users.get(0);
      }

      return null;
   }

   @Override
   public List<User> findAll() {
      Session session = sessionFactory.withOptions().tenantIdentifier(tenant).openSession();
      return session.createCriteria(User.class).list();
   }

   @Override
   public void populate() {
      Session session = sessionFactory.withOptions().tenantIdentifier(tenant).openSession();
      Transaction transaction = session.getTransaction();

      transaction.begin();
      session.createQuery("DROP ALL OBJECTS").executeUpdate();
      session.createSQLQuery("create table User (id serial primary key, logn varchar(50) not null, pass varchar(50) not null)").executeUpdate();
      User generatedUser = generateEntityForTenant(tenant);
      save(generatedUser);
   }

   private User generateEntityForTenant(String forTenant) {
      User user = new User();
      user.setUsername("admin");
      user.setPassword("admin");
      //return new User("admin", "admin");

      return user;
   }
}
