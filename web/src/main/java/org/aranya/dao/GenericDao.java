package org.aranya.dao;

import java.util.List;

public interface GenericDao<T> {
   void save (T entity);
   void delete (T entity);
   T findByName(String name);
   List<T> findAll();
   void populate();
}
