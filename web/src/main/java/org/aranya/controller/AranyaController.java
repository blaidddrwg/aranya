package org.aranya.controller;

import org.aranya.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class AranyaController {
   
   @RequestMapping(value = "/", method = RequestMethod.GET)
   public String login() {
      return "login";
   }
}
