$(function(){
   function initMap(){
      var $map = $('#map'),
      state;
      $map.mapael({
            map:{
               name : "india_states",
               defaultArea : {
                  attrsHover : {
                     fill : '#242424',
                     animDuration : 100
                  },
                  tooltip: {
                     content: function(){
                        return '<strong>' + state + '</strong>';
                     }
                  },
                  eventHandlers: {
                     mouseover: function(e, id){
                        state = id;
                     }
                  }
               },
               defaultPlot:{
                  size: 7,
                  attrs : {
                     fill : Sing.colors['brand-success'],
                     stroke : "#fff",
                     "stroke-width" : 0,
                     "stroke-linejoin" : "round"
                  },
                  attrsHover : {
                     "stroke-width" : 1,
                     animDuration : 100
                  }
               },
               zoom : {
                  enabled : true,
                  step : 0.75
               }
            },
            plots:{
               'aranya' : {
                  latitude: 12.9719400,
                  longitude: 77.5936900,
                  size: 9,
                  attrs: { fill: Sing.colors['brand-danger'] },
                  tooltip: {content : "Aranya Base"}
               },
               'bmp_nursery' : {
                  latitude: 12.914976,
                  longitude: 77.400265,
                  size: 3,
                  tooltip: {content : "BMP Nursery"}
               },
               'siddedevarabetta' : {
                  latitude: 12.881260,
                  longitude: 77.243990,
                  size: 5,
                  tooltip: {content : "Siddedevarabetta Forest Project"}
               },
               'pushpagiri' : {
                  latitude: 12.476740,
                  longitude: 75.382060,
                  size: 13,
                  tooltip: {content : "Pushpagiri Forest Project"}
               },
               'punajur' : {
                  latitude: 11.844711,
                  longitude: 77.111437,
                  size: 15,
                  tooltip: {content : "Punajur Forest Project"}
               },
               'koundinya' : {
                  latitude: 13.020930,
                  longitude: 78.592331,
                  size: 11,
                  tooltip: {content : "Koundinya Forest Project"}
               },
               'nagapuri' : {
                  latitude: 13.373502,
                  longitude: 76.277359,
                  size: 19,
                  tooltip: {content : "Nagapuri Forest Project"}
               },
               'someshwara' : {
                  latitude: 13.392601,
                  longitude: 75.080991,
                  size: 8,
                  tooltip: {content : "Someshwara Forest Project"}
               },
               'mookambika' : {
                  latitude: 13.794436,
                  longitude: 74.841948,
                  tooltip: {content : "Mookambika Forest Project"}
               },
               'sharavathi' : {
                  latitude: 14.084719,
                  longitude: 74.837329,
                  size: 11,
                  attrs: { fill: Sing.colors['brand-warning'] },
                  tooltip: {content : "Sharavathi Valley Project"}
               },
               'ranebennur' : {
                  latitude: 14.653621,
                  longitude: 75.664272,
                  tooltip: {content : "Rannebennur Sanctury Project"}
               },
               'gonahal' : {
                  latitude: 15.259733,
                  longitude: 76.618291,
                  size : 7,
                  attrs: { fill: Sing.colors['brand-danger'] },
                  tooltip: {content : "Daroji Sanctury Project"}
               },
               'moka' : {
                  latitude: 15.243594, 
                  longitude: 77.075895,
                  size : 5,
                  tooltip: {content : "Moka Reserve Forest Project"}
               },
               'bidar' : {
                  latitude: 17.936116,
                  longitude: 77.502076,
                  size : 7,
                  attrs : { fill: Sing.colors['brand-primary']
                  },
                  tooltip: {content : "Bidar Regreening Project"}
               }
            }
      });

      //ie svg height fix
      function _fixMapHeight(){
         $map.find('svg').css('height', function(){
            return $(this).attr('height') + 'px';
         });
      }

      _fixMapHeight();
      SingApp.onResize(function(){
         setTimeout(function(){
            _fixMapHeight();
         }, 100)
      });
   }

   function initCalendar(){
      var monthNames = ["January", "February", "March", "April", "May", "June",  "July", "August", "September", "October", "November", "December"];
      var dayNames = ["S", "M", "T", "W", "T", "F", "S"];

      var now = new Date(),
      month = now.getMonth() + 1,
      year = now.getFullYear();

      var events = [
         [
            "2/"+month+"/"+year,
            'The flower bed',
            '#',
            Sing.colors['brand-primary'],
            'Contents here'
            ],
      [
         "5/"+month+"/"+year,
         'Stop world water pollution',
         '#',
         Sing.colors['brand-warning'],
         'Have a kick off meeting with .inc company'
            ],
            [
               "18/"+month+"/"+year,
               'Light Blue 2.2 release',
               '#',
               Sing.colors['brand-success'],
               'Some contents here'
            ],
            [
               "29/"+month+"/"+year,
               'A link',
               'http://www.flatlogic.com',
               Sing.colors['brand-danger']
            ]
        ];
        var $calendar = $('#events-calendar');
        $calendar.calendar({
              months: monthNames,
              days: dayNames,
              events: events,
              popover_options:{
                 placement: 'top',
                 html: true
              }
        });
        $calendar.find('.icon-arrow-left').addClass('fa fa-arrow-left');
        $calendar.find('.icon-arrow-right').addClass('fa fa-arrow-right');
        function restyleCalendar(){
           $calendar.find('.event').each(function(){
              var $this = $(this),
              $eventIndicator = $('<span></span>');
              $eventIndicator.css('background-color', $this.css('background-color')).appendTo($this.find('a'));
              $this.css('background-color', '');
           })
        }
        $calendar.find('.icon-arrow-left, .icon-arrow-right').parent().on('click', restyleCalendar);
        restyleCalendar();
   }

   function initRickshaw(){
      "use strict";

      var seriesData = [ [], [] ];
      var random = new Rickshaw.Fixtures.RandomData(30);

      for (var i = 0; i < 30; i++) {
         random.addData(seriesData);
      }

      var graph = new Rickshaw.Graph( {
            element: document.getElementById("rickshaw"),
            height: 100,
            renderer: 'area',
            series: [
               {
                  color: '#F7653F',
                  data: seriesData[0],
                  name: 'Uploads'
               }, {
                  color: '#F7D9C5',
                  data: seriesData[1],
                  name: 'Downloads'
               }
            ]
      } );

      function onResize(){
         var $chart = $('#rickshaw');
         graph.configure({
               width: $chart.width(),
               height: 100
         });
         graph.render();

         $chart.find('svg').css({height: '100px'})
      }

      SingApp.onResize(onResize);
      onResize();


      var hoverDetail = new Rickshaw.Graph.HoverDetail( {
            graph: graph,
            xFormatter: function(x) {
               return new Date(x * 1000).toString();
            }
      } );

      setInterval( function() {
         random.removeData(seriesData);
         random.addData(seriesData);
         graph.update();

      }, 1000 );
   }

   function initAnimations(){
      $('#geo-locations-number, #percent-1, #percent-2, #percent-3').each(function(){
         $(this).animateNumber({
               number: $(this).text().replace(/ /gi, ''),
               numberStep: $.animateNumber.numberStepFactories.separator(' '),
               easing: 'easeInQuad'
         }, 1000);
      });

      $('.js-progress-animate').animateProgressBar();
   }

   function pjaxPageLoad(){
      $('.widget').widgster();
      initMap();
      //initCalendar();
      //initRickshaw();
      initAnimations();
   }

   pjaxPageLoad();
   SingApp.onPageLoad(pjaxPageLoad);

});
