var oldAttributes;

function initGMap() {
   var bangalore = {lat: 12.972442, lng: 77.580643};
   var map = new google.maps.Map(document.getElementById('tcgmap'), {
      zoom: 10,
      center: bangalore
   });
   var marker = new google.maps.Marker({
      position: bangalore,
      map: map
   });
}

function setPartner(partnerId, partnerName, campaignCount, locationCount) {
   selected.part.id = partnerId;
   selected.part.name = partnerName;
   selected.camp.count = campaignCount;
   selected.locn.count = locationCount;
   selected.locn.ne = 0;

   // Fill up the address in the Billing Address section and open it..
   var add1 = document.getElementById('add1');
   add1.value = '801 Bannerghatta Main Rd';

   var city = document.getElementById('city');
   city.value = 'Bangalore';

   var state = document.getElementById('state');
   state.value = 'KA';

   var zip = document.getElementById('zip');
   zip.value = '560 017';

   showMap();
}

function showCampaigns() {
   document.getElementById('c' + selected.part.id).click();
}

function showLocations() {
   document.getElementById('l' + selected.part.id).click();
}

function addPartner() {
   var tbody = document.getElementById('client-list');

   var row = tbody.insertRow(0);
   row.setAttribute('class', 'alert alert-success');

   var idCell = row.insertCell(0);
   var cellIdData = document.createElement('span');
   var cellIcon = document.createElement('i');
   cellIcon.setAttribute('class', 'glyphicon glyphicon-cog');
   cellIcon.setAttribute('style', 'vertical-align: middle');
   cellIdData.appendChild(cellIcon);
   idCell.appendChild(cellIdData);

   var cellCN = row.insertCell(1);
   var cellClientName = document.createElement('input');
   cellClientName.setAttribute('class', 'form-control');
   cellClientName.setAttribute('placeholder', 'Client name');
   cellClientName.setAttribute('type', 'text');
   cellClientName.setAttribute('id', 'newClientName');
   cellCN.appendChild(cellClientName);

   var cellLoc = row.insertCell(2);
   var cellClientLoc = document.createElement('input');
   cellClientLoc.setAttribute('class', 'form-control');
   cellClientLoc.setAttribute('placeholder', 'Client location');
   cellClientLoc.setAttribute('type', 'text');
   cellClientLoc.setAttribute('id', 'newClientLoc');
   cellLoc.appendChild(cellClientLoc);

   var cellSites = row.insertCell(3);
   cellSiteCount = document.createElement('input');
   cellSiteCount.setAttribute('class', 'form-control');
   cellSiteCount.setAttribute('placeholder', '# Sites');
   cellSiteCount.setAttribute('type', 'text');
   cellSiteCount.setAttribute('maxlength', '2');
   cellSiteCount.setAttribute('size', '2');
   cellSiteCount.setAttribute('id', 'newClientSiteCount');
   cellSites.appendChild(cellSiteCount);

   var cellInstalls = row.insertCell(4);
   cellInstCount = document.createElement('input');
   cellInstCount.setAttribute('class', 'form-control');
   cellInstCount.setAttribute('placeholder', '# Installs');
   cellInstCount.setAttribute('type', 'text');
   cellInstCount.setAttribute('maxlength', '2');
   cellInstCount.setAttribute('size', '2');
   cellInstCount.setAttribute('id', 'newClientInstallCount');
   cellInstalls.appendChild(cellInstCount);

   var cellStatus = row.insertCell(5);
   var buttonToolbar = document.createElement('div');
   buttonToolbar.setAttribute('class', 'btn-toolbar');

   var submitButton = document.createElement('input');
   submitButton.setAttribute('class', 'btn btn-sm btn-success pull-left');
   submitButton.type = 'button';
   submitButton.value = 'Save';
   submitButton.onclick = function() {
      idCell.removeChild(cellIdData);
      var newClientName = cellClientName.value;
      cellCN.removeChild(cellClientName);
      cellCN.appendChild(document.createTextNode(newClientName + ' '));
      var newClientMarker = document.createElement('span');
      newClientMarker.setAttribute('class', 'label label-success text-gray-dark');
      newClientMarker.appendChild(document.createTextNode('New'));
      cellCN.appendChild(newClientMarker);
      var newLoc = cellClientLoc.value;
      cellLoc.removeChild(cellClientLoc);
      cellLoc.appendChild(document.createTextNode(newLoc));
      var newSiteCount = cellSiteCount.value;
      cellSites.removeChild(cellSiteCount);
      cellSites.appendChild(document.createTextNode(newSiteCount));
      var newInstCount = cellInstCount.value;
      cellInstalls.removeChild(cellInstCount);
      cellInstalls.appendChild(document.createTextNode(newInstCount));
      cellStatus.removeChild(buttonToolbar);

      row.removeAttribute('class');
   };

   buttonToolbar.appendChild(submitButton);

   var cancelButton = document.createElement('input');
   cancelButton.setAttribute('class', 'btn btn-sm btn-danger pull-left');
   cancelButton.type = 'button';
   cancelButton.value = 'Cancel';
   cancelButton.onclick = function() {
      tbody.removeChild(row);
   };
   buttonToolbar.appendChild(cancelButton);
   cellStatus.appendChild(buttonToolbar);

}

/*
$('#client-table').on('click', '.clickable-row', function(event) {
   alert('Row clicked [' + $(this) + ']');
   $(this).addClass('highlight').siblings().removeClass('highlight');
});
*/
