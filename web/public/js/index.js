var selected = {
   part: {id: '', name: ''},
   camp: {id: '', name: '', locn : '', count: '', ne: ''},
   locn: {id: '', name: '',            count: '', ne: ''},
   tree: {id: '', name: '', spid : '', count: '', ne: ''},
};

$(function() {
   $("#content").load("dashboard.html");
});

function randomNumber(max) {
   return Math.floor((Math.random() * max) + 1);
}


function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function showMap() {
   var map = document.getElementById('tcgmap');
   map.style.visibility = 'visible';
}

