var soils = {
   data: [
      {id: "1",  name: "Drought Prone Uplands (Sandy Soil)"},
      {id: "2",  name: "Well Drained Uplands (Sandy to Loamy Soil)"},
      {id: "3",  name: "Moderately Drained Uplands (Silt to Clay Soils)"},
      {id: "4",  name: "Poorly Drained Uplands (Clay or Impeded Drainage Soils)"},
      {id: "5",  name: "Poorly Drained Low Lands (Swamps to Riparian Mineral or Organic Soils)"},
      {id: "6",  name: "Poorly Drained Low Lands (Sedge to Conifer Bogs - Organic Soils)"},
   ]
};
