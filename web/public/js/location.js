function replaceHeaderByline() {
   if(selected.part.id != undefined) {
      var subheading = document.getElementById('sub-heading');
      subheading.innerHTML = selected.part.name;
   }
}

function loadLocations() {
   var tbody = document.getElementById('sites-data');

   for(var i = 0; i < selected.locn.count; i++) {
      var randomLoc = loct.data[randomNumber(20)];
      var shouldShowError = (Math.floor((Math.random() * 10) + 1) == 5);
      var isNew = ((randomNumber(7) == 4) && ((i == 0) || (i == 1) || (i == 2))); // Some random magic to generate success probabistically...

      var row = tbody.insertRow(tbody.rows.length);

      if(!isNew && shouldShowError) {
         row.setAttribute('class', 'alert alert-danger');
      }

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var nameCell = row.insertCell(1);
      cellData = document.createTextNode(randomLoc.name + ' ');
      nameCell.appendChild(cellData);
      if(isNew) {
         var newLabelSpan = document.createElement('span');
         newLabelSpan.setAttribute('class', 'label label-success');
         var newBadge = document.createTextNode('New/Survey Requested');
         newLabelSpan.appendChild(newBadge);
         nameCell.appendChild(newLabelSpan);
      }

      var locCell = row.insertCell(2);
      cellData = document.createTextNode(selected.part.name);
      locCell.appendChild(cellData);

      var clientCell = row.insertCell(3);
      cellData = document.createTextNode(randomLoc.loc);
      clientCell.appendChild(cellData);

      var instCell = row.insertCell(4);
      var instCount = randomNumber(30)*200;
      row.setAttribute('onClick', "setLocation (" + (i + 1) + ", " + instCount + ", '" + randomLoc.name + "', '" + randomLoc.loc + "');");

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 's' + (i + 1));
      dataRef.setAttribute('data-ajax-load', 'trees.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");

      if((!isNew) && shouldShowError) {
         var dataSpan = document.createElement('span');
         dataSpan.setAttribute('class', 'label label-danger text-gray-dark text-align-center');

         cellData = document.createTextNode((instCount - 1) + '/' + instCount);
         dataRef.appendChild(cellData);
         dataSpan.appendChild(dataRef);
         instCell.appendChild(dataSpan);
      }
      else {
         if(isNew) {
            cellData = document.createTextNode(instCount);
         }
         else {
            cellData = document.createTextNode(instCount + '/' + instCount);
         }
         dataRef.appendChild(cellData);
         instCell.appendChild(dataRef);
      }

      var statCell = row.insertCell(5);
      if(shouldShowError) {
         var alertType = Math.floor((Math.random() * 2) + 1);
         if(alertType == 1) {
            cellData = document.createTextNode('Drought alert received');
         }
         else {
            cellData = document.createTextNode('Flood alert received');
         }
      }
      else {
         var lastHeardFrom = Math.floor((Math.random() * 30) + 1);
         cellData = document.createTextNode('Last actionable event ' + lastHeardFrom + ' days ago');
      }
      statCell.appendChild(cellData);
   }
}

function setLocation(locnId, treeCount,  locName, locLocation) {
   selected.locn.id = locnId;
   selected.locn.name = locName;
   selected.locn.loc = locLocation;
   selected.locn.count = treeCount;

   // Fill up info in the appropriate fields..
   var locationName = document.getElementById('locName');
   locationName.innerHTML = locName;

   // Fill in a random number of trees in the 'details' section...
   var tbody = document.getElementById('location-details');

   // First clear all rows..
   tbody.innerHTML = "";

   var totalTreeCount = 0;
   for(var i = 0; totalTreeCount < treeCount; i++) {
      var speciesTreeCount = randomNumber(1000);
      var speciesIndex = randomNumber(species.data.length - 1);
      var selectedSpecies = species.data[speciesIndex];

      var row = tbody.insertRow(tbody.rows.length);

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var speciesBotNameCell = row.insertCell(1);
      cellData = document.createTextNode(selectedSpecies.botn);
      speciesBotNameCell.appendChild(cellData);

      var speciesCommonNameCell = row.insertCell(2);
      cellData = document.createTextNode(selectedSpecies.name);
      speciesCommonNameCell.appendChild(cellData);

      var soilIndex = randomNumber(soils.data.length - 1);
      var selectedSoil = soils.data[soilIndex];
      var soilCell = row.insertCell(3);
      cellData = document.createTextNode(selectedSoil.name);
      soilCell.appendChild(cellData);

      if((speciesTreeCount + totalTreeCount) > treeCount) {
         speciesTreeCount = (treeCount - totalTreeCount);
      }

      var plantedCell = row.insertCell(4);

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 't' + (i + 1));
      dataRef.setAttribute('data-ajax-load', 'trees.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");
      cellData = document.createTextNode(speciesTreeCount);
      dataRef.appendChild(cellData);
      plantedCell.appendChild(dataRef);

      var survivedCell = row.insertCell(5);
      cellData = document.createTextNode(speciesTreeCount);
      survivedCell.appendChild(cellData);

      row.setAttribute('onClick', "showTrees ("  + (i + 1) + ", " + speciesIndex + ", " + speciesTreeCount + ");");
      totalTreeCount += speciesTreeCount;
   }
}

function addLocation() {
   document.getElementById('s' + siteId).click();
}

function showTrees(rowIndex, speciesIndex, speciesTreeCount) {
   selected.tree.count = speciesTreeCount;
   selected.tree.spid = speciesIndex;

   document.getElementById('t' + rowIndex).click();
}

replaceHeaderByline();
loadLocations();
