function replaceHeaderByline() {
   if(selected.part.id != undefined) {
      var subheading = document.getElementById('sub-heading');
      subheading.innerHTML = selected.part.name + ', ' + selected.camp.name;
   }
}

function loadTrees() {
   var tbody = document.getElementById('sites-data');
   for(var i = 0; i < selected.tree.count; i++) {
      var shouldShowError = (Math.floor((Math.random() * 10) + 1) == 5);

      var row = tbody.insertRow(tbody.rows.length);
      if(shouldShowError) {
         row.setAttribute('class', 'alert alert-danger');
      }

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var UuidCell = row.insertCell(1);
      var treeUUID = uuidv4();
      cellData = document.createTextNode(treeUUID);
      UuidCell.appendChild(cellData);

      var botnCell = row.insertCell(2);
      cellData = document.createTextNode(species.data[selected.tree.spid].botn);
      botnCell.appendChild(cellData);

      var nameCell = row.insertCell(3);
      cellData = document.createTextNode(species.data[selected.tree.spid].name);
      nameCell.appendChild(cellData);

      var randomLoc = Math.floor((Math.random() * loct.data.length));
      var locCell = row.insertCell(4);
      cellData = document.createTextNode(loct.data[randomLoc].name);
      locCell.appendChild(cellData);

      var campCell = row.insertCell(5);
      cellData = document.createTextNode(selected.camp.name);
      campCell.appendChild(cellData);
      row.setAttribute('onClick', "showTree(" + i + ", '" + treeUUID + "', " + selected.tree.spid + ", " + randomLoc + ");");

      var statCell = row.insertCell(6);
      if(shouldShowError) {
         var alertType = Math.floor((Math.random() * 2) + 1);
         if(alertType == 1) {
            cellData = document.createTextNode('Moisture alert received');
         }
         else {
            cellData = document.createTextNode('Hardware alert received');
         }
      }
      else {
         var lastHeardFrom = Math.floor((Math.random() * 30) + 1);
         cellData = document.createTextNode('Last actionable event ' + lastHeardFrom + ' days ago');
      }
      statCell.appendChild(cellData);
   }
}

var imageIndex = 0;
function showTree(treeId, treeUUID, speciesId, treeLocation) {
   selected.tree.id = treeUUID;
   var treeDetails = document.getElementById('treeId');
   treeDetails.innerHTML = treeUUID

   var speciesSelector = document.getElementById('treeSpecies');
   speciesSelector.options[speciesId].selected = true;

   var imageCarousel = document.getElementById('treeImage');
   imageCarousel.visibility = "visible";

   var treeImageList = document.getElementById('treeImageList');
   treeImageList.innerHTML = '';

   imageIndex = 0;
   showNextImageInTreeCarousel();
}

function showNextImageInTreeCarousel() {
   var treeImageList = document.getElementById('treeImageList');

   // Set the existing element to NOT 'active' so as not to confuse the carousel...
   for(var index = 0; index < treeImageList.children.length; index++) {
      treeImageList.children[index].setAttribute('class', 'carousel-item');
   }

   var treeImage = document.createElement('img');
   treeImage.setAttribute('class', 'd-block w-100');
   imageIndex++;
   treeImage.setAttribute('class', 'carousel-item active');
   treeImage.setAttribute('src', 'img/tree/' + imageIndex + '.jpg');
   treeImage.setAttribute('alt', 'Photo taken on ' + randomDate(imageIndex));

   var treeImageDiv = document.createElement('div');
   treeImageDiv.appendChild(treeImage);
   treeImageList.appendChild(treeImageDiv);
}

function showPrevImageInTreeCarousel() {
   var treeImageList = document.getElementById('treeImageList');

   if(treeImageList.hasChildNodes() && imageIndex > 1) {
      for(var index = 0; index < treeImageList.children.length; index++) {
         treeImageList.children[index].setAttribute('class', 'carousel-item');
      }

      imageIndex = imageIndex - 1;
      treeImageList.children[imageIndex - 1].setAttribute('class', 'carousel-item active');
   }
}

function randomDate(yearNumber) {
   return '2016';
}

replaceHeaderByline();
loadTrees();
