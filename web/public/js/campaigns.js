var campaign = {
   data: [
      {id: "1",  name: "Urban Reforestation Project 2016",     loc: "Bangalore, KA"},
      {id: "2",  name: "MAMS Project 2016",                    loc: "Bangalore, KA"},
      {id: "3",  name: "BHU Project 2017",                     loc:"Benares, UP"},
      {id: "4",  name: "Vindhya Regreening 2016",              loc: "Vindhya, KA"},
      {id: "5",  name: "SBA Initiative  2016",                 loc: "Bangalore, KA"},
      {id: "6",  name: "Urban Green Buffer Renewal",           loc: "Bangalore, KA"},
      {id: "7",  name: "Green Bangalore Project 2015",         loc: "Bangalore, KA"},
      {id: "8",  name: "DMWL Tree Plantation Program",         loc: "Rannebennur, KA"},
      {id: "9",  name: "Tumkur Revival Project 2016",          loc: "Tumkur, KA"},
      {id: "10", name: "Hasan Climate Control Test",           loc: "Hasan, KA"},
      {id: "11", name: "IISC Tree Planting Program 2015",      loc: "Bangalore, KA"},
      {id: "12", name: "Magdi Road Shade Program",             loc: "Bangalore, KA"},
      {id: "13", name: "Nagapuri Afforestation Program",       loc: "Nagapuri, KA"},
      {id: "14", name: "Koundinya Renewal 2016",               loc: "Koundinya, KA"},
      {id: "15", name: "Bidar Regreening Project 2016",        loc: "Bidar, KA"},
      {id: "16", name: "Bidar Urban Buffer Program",           loc: "Bidar, KA"},
      {id: "17", name: "Daroji Renewal Program",               loc: "Daroji, KA"},
      {id: "18", name: "Moka Renewal Program",                 loc: "Moka, KA"},
      {id: "19", name: "Sharavathi Watershed Renewal",         loc: "Sagara, KA"},
      {id: "20", name: "Urban Reforestation Project 2017",     loc: "Bangalore, KA"},
      {id: "21", name: "Vindhya Regreening 2017",              loc: "Vindhya, KA"},
      {id: "22", name: "SBA Initiative 2017",                  loc: "Bangalore, KA"},
      {id: "23", name: "Green Bangalore Project 2016",         loc: "Bangalore, KA"},
      {id: "24", name: "Green Bangalore Project 2017",         loc: "Bangalore, KA"},
      {id: "25", name: "Koundinya Renewal 2017",               loc: "Koundinya, KA"},
      {id: "26", name: "Tumkur Revival Project 2017",          loc: "Tumkur, KA"},
      {id: "27", name: "BHU Project 2016",                     loc: "Benares, UP"},
      {id: "28", name: "IISC Tree Planting Program 2016",      loc: "Bangalore, KA"},
      {id: "29", name: "IISC Tree Planting Program 2017",      loc: "Bangalore, KA"},
      {id: "30", name: "Bidar Regreening Project 2017",        loc: "Bidar, KA"},
   ]
};

function replaceHeaderByline() {
   if(selected.part.id != undefined) {
      var subheading = document.getElementById('sub-heading');
      subheading.innerHTML = selected.part.name;
   }
}

function loadCampaigns() {
   var tbody = document.getElementById('sites-data');

   for(var i = 0; i < selected.camp.count; i++) {
      var randomCamp = campaign.data[randomNumber(29)];
      var shouldShowError = (Math.floor((Math.random() * 10) + 1) == 5);
      var isNew = ((randomNumber(7) == 4) && ((i == 0) || (i == 1) || (i == 2))); // Some random magic to generate success probabistically...

      var row = tbody.insertRow(tbody.rows.length);

      if(!isNew && shouldShowError) {
         row.setAttribute('class', 'alert alert-danger');
      }

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var nameCell = row.insertCell(1);
      cellData = document.createTextNode(randomCamp.name + ' ');
      nameCell.appendChild(cellData);
      if(isNew) {
         var newLabelSpan = document.createElement('span');
         newLabelSpan.setAttribute('class', 'label label-success');
         var newBadge = document.createTextNode('New/Survey Requested');
         newLabelSpan.appendChild(newBadge);
         nameCell.appendChild(newLabelSpan);
      }

      var locCell = row.insertCell(2);
      cellData = document.createTextNode(selected.part.name);
      locCell.appendChild(cellData);

      var clientCell = row.insertCell(3);
      cellData = document.createTextNode(randomCamp.loc);
      clientCell.appendChild(cellData);

      var instCell = row.insertCell(4);
      var instCount = randomNumber(30)*200;
      row.setAttribute('onClick', "setCampaign (" + (i + 1) + ", " + instCount + ", '" + randomCamp.name + "', '" + randomCamp.loc + "');");

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 's' + (i + 1));
      dataRef.setAttribute('data-ajax-load', 'trees.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");

      if((!isNew) && shouldShowError) {
         var dataSpan = document.createElement('span');
         dataSpan.setAttribute('class', 'label label-danger text-gray-dark text-align-center');

         cellData = document.createTextNode((instCount - 1) + '/' + instCount);
         dataRef.appendChild(cellData);
         dataSpan.appendChild(dataRef);
         instCell.appendChild(dataSpan);
      }
      else {
         if(isNew) {
            cellData = document.createTextNode(instCount);
         }
         else {
            cellData = document.createTextNode(instCount + '/' + instCount);
         }
         dataRef.appendChild(cellData);
         instCell.appendChild(dataRef);
      }

      var statCell = row.insertCell(5);
      if(shouldShowError) {
         var alertType = Math.floor((Math.random() * 2) + 1);
         if(alertType == 1) {
            cellData = document.createTextNode('Drought alert received');
         }
         else {
            cellData = document.createTextNode('Flood alert received');
         }
      }
      else {
         var lastHeardFrom = Math.floor((Math.random() * 30) + 1);
         cellData = document.createTextNode('Last actionable event ' + lastHeardFrom + ' days ago');
      }
      statCell.appendChild(cellData);
   }
}

function setCampaign(campId, treeCount,  campName, campLocation) {
   selected.camp.id = campId;
   selected.camp.name = campName;
   selected.camp.loc = campLocation;
   selected.tree.count = treeCount;

   // Fill up info in the appropriate fields..
   var campaignName = document.getElementById('campName');
   campaignName.innerHTML = campName;

   // Fill in a random number of trees in the 'details' section...
   var tbody = document.getElementById('campaign-details');

   // First clear all rows..
   tbody.innerHTML = "";

   var totalTreeCount = 0;
   for(var i = 0; totalTreeCount < treeCount; i++) {
      var speciesTreeCount = randomNumber(1000);
      var speciesIndex = randomNumber(species.data.length - 1);
      var selectedSpecies = species.data[speciesIndex];

      var row = tbody.insertRow(tbody.rows.length);

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var speciesBotNameCell = row.insertCell(1);
      cellData = document.createTextNode(selectedSpecies.botn);
      speciesBotNameCell.appendChild(cellData);

      var speciesCommonNameCell = row.insertCell(2);
      cellData = document.createTextNode(selectedSpecies.name);
      speciesCommonNameCell.appendChild(cellData);

      var soilIndex = randomNumber(soils.data.length - 1);
      var selectedSoil = soils.data[soilIndex];
      var soilCell = row.insertCell(3);
      cellData = document.createTextNode(selectedSoil.name);
      soilCell.appendChild(cellData);

      if((speciesTreeCount + totalTreeCount) > treeCount) {
         speciesTreeCount = (treeCount - totalTreeCount);
      }

      var plantedCell = row.insertCell(4);

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 't' + (i + 1));
      dataRef.setAttribute('data-ajax-load', 'trees.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");
      cellData = document.createTextNode(speciesTreeCount);
      dataRef.appendChild(cellData);
      plantedCell.appendChild(dataRef);

      var survivedCell = row.insertCell(5);
      cellData = document.createTextNode(speciesTreeCount);
      survivedCell.appendChild(cellData);

      row.setAttribute('onClick', "showTrees ("  + (i + 1) + ", " + speciesIndex + ", " + speciesTreeCount + ");");
      totalTreeCount += speciesTreeCount;
   }
}

function addCampaign() {
   document.getElementById('s' + siteId).click();
}

function showTrees(rowIndex, speciesIndex, speciesTreeCount) {
   selected.tree.count = speciesTreeCount;
   selected.tree.spid = speciesIndex;

   document.getElementById('t' + rowIndex).click();
}

replaceHeaderByline();
loadCampaigns();
